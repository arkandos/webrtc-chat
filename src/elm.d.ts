declare module "*.elm" {
    interface ElmPort {
        subscribe(callback: (data: any) => void): void;
        send(data: unknown): void;
    }

    interface ElmAppInstance {
        ports: {
            [portName: string]: ElmPort;
        }

        die(): void;
    }

    interface ElmInitOptions {
        node: HTMLElement
        flags?: unknown
    }

    interface ElmConstructor {
        init(options: ElmInitOptions): ElmAppInstance;
    }

    export const Elm: {
        [name: string]: ElmConstructor;
    }
}
