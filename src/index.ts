import { type Channel } from 'pusher-js'
import { createPeerConnection, createPusherSignalingChannel } from '@jreusch/webrtc'

import './main.css'
import { Elm } from './Main.elm'

const app = Elm.Main.init({
    node: document.body
})

let connection: RTCPeerConnection | null
app.ports.connect.subscribe((uuid: string) => {
    let channel: Channel
    createPeerConnection({
        signaling: createPusherSignalingChannel(channel)
    })
})
