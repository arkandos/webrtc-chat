module Main exposing (main)

import Browser exposing (Document, UrlRequest)
import Browser.Navigation
import Html exposing (Html)
import Url exposing (Url)


type alias Flags =
    ()


type alias Model =
    { navKey : Browser.Navigation.Key
    , url : Url
    }


type Msg
    = UrlChanged Url
    | LinkClicked UrlRequest


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }


init : Flags -> Url -> Browser.Navigation.Key -> ( Model, Cmd Msg )
init () url navKey =
    ( { navKey = navKey
      , url = url
      }
    , Cmd.none
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        UrlChanged url ->
            ( { model | url = url }, Cmd.none )

        LinkClicked (Browser.Internal url) ->
            ( model, Browser.Navigation.pushUrl model.navKey (Url.toString url) )

        LinkClicked (Browser.External url) ->
            ( model, Browser.Navigation.load url )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


view : Model -> Document Msg
view model =
    { title = "WebRTC Chat"
    , body =
        [ Html.text "Hello, Sailor!"
        ]
    }
