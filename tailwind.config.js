/** @type {import('tailwindcss').Config} */
export default {
  content: [
    './index.html',
    './src/**/*.{js,ts,elm}',
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}

